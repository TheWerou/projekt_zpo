﻿using System;

namespace MasterMind
{
    /// <summary>
    /// Class that represent user
    /// </summary>
    public class User
    {
        // Wszedzie dodac sprawdzanie danych
        private string nazwa; ///
        private int pkt;

        //----------------------------------------------------- Construktors
        public User(string name) // constructor with changable name
        {
            this.nazwa = name;
            
        }
        public User() // const defoult
        {
            this.nazwa = "Gracz";
        }
        //-------------------------------------------------------- seters
        /// <summary>
        /// Methods adds 1 pkt to user
        /// </summary>
        public void add_pkt()// update int pkt
        {
            this.pkt += 1;
            
        }
        /// <summary>
        /// Method give us amount of pkt
        /// </summary>
        /// <returns>int >= 0</returns>
        public int see_pkt() // return pkts
        {
            return this.pkt;
        }
        //--------------------------------------------------------getters
        /// <summary>
        /// give us name of user
        /// </summary>
        /// <returns> string </returns>
        public string see_name()// return name of users
        { 
            return this.nazwa;
        }
        //-------------------------------------------------------- others
    }
}

