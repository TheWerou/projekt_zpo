﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterMind
{
    /// <summary>
    /// List container and methods for it
    /// </summary>
    public class BaseContainer
    {
        private List<Array> container;
        private bool changable;
        private int amount_can_be;
        public BaseContainer()
        {
            this.changable = true;
            this.amount_can_be = 10;
            this.container = new List<Array>();
        }
        public BaseContainer(int amount)
        {
            this.changable = true;
            this.amount_can_be = amount;
            this.container = new List<Array>();
        }
        /// <summary>
        /// adds array to list
        /// </summary>
        /// <param name="array_to_add">basicly it is what it writes</param>
        public void add(Array array_to_add) // add something to oceny list ,must be a array
        {
            int helper = 0;


            if(this.changable is true)
            {
                if (array_to_add is Array)
                {
                    foreach (int element in array_to_add)
                    {
                        helper += 1;

                    }

                    if (helper == 4)
                    {
                        this.container.Add(array_to_add);
                        this.amount_check();
                    }
                    else
                    {
                        throw new Exception("at least one element of array to add is not a int");
                    }

                }
                else
                {
                    throw new Exception("Obiect is not an array");
                }
            }
            else
            {
                Console.WriteLine(this.amount_can_be + this.lenght());
                Console.WriteLine(this.lenght());
                throw new Exception("You can not add anything to this container");
            }


        }
        /// <summary>
        /// give us back one array from list 
        /// </summary>
        /// <param name="num">number of array from list</param>
        /// <returns></returns>
        public Array get_one(int num) // gives u one element from oceny list
        {
            if (num >= 0)
            {
                return this.container[num];
            }
            else
            {
                throw new Exception("object is not in list");
            }
        }
        /// <summary>
        /// returns lenght of list
        /// </summary>
        /// <returns> int </returns>
        public int lenght()//return lenght of oceny list
        {
            int lght = this.container.Count;
            return lght;
        }
        /// <summary>
        /// deletes all arrays from list
        /// </summary>
        public void reset_all()
        {
            this.container.Clear();
            this.amount_check();
        }
        /// <summary>
        /// returns whole list
        /// </summary>
        /// <returns> List<Array> </returns>
        public List<Array> return_all()//returns u a glowny kot
        {
            return this.container;
        }
        /// <summary>
        /// check if u can add one more array
        /// </summary>
        /// <returns></returns>
        public bool can_i_add()
        {
            if (this.changable == true)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        /// <summary>
        /// private method check if u can add array 
        /// </summary>
        /// <returns>boll</returns>
        private bool amount_check()
        {
            if(this.amount_can_be < this.lenght())
            {
                
                this.changable = false;
                return false;
            }
            else
            {
                
                this.changable = true;
                return true;
            }
            
        }
    }
}
