﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterMind
{
    /// <summary>
    /// Base dictionary class and methods
    /// </summary>
    public class DictionaryClass
    {
        private Dictionary<int, string> dict;

        public DictionaryClass()
        {
            this.dict = new Dictionary<int, string>();
        }
        /// <summary>
        /// return string from dictionary specified by passed number(key)
        /// </summary>
        /// <param name="key"> key to dictionary</param>
        /// <returns>string</returns>
        public string ret_by_string(int key)
        {
            return dict[key];
        }
        /// <summary>
        /// lenhgt of dictionary
        /// </summary>
        /// <returns>int</returns>
        public int lenght()
        {
            return dict.Count;
        }
        /// <summary>
        /// add something do dictionary
        /// </summary>
        /// <param name="i">key</param>
        /// <param name="color">string</param>
        public void add(int i,string color)
        {
            dict.Add(i, color);
        }
        /// <summary>
        /// clear dictionary
        /// </summary>
        public void reset_all()
        {
            this.dict.Clear();
        }
    }
}
