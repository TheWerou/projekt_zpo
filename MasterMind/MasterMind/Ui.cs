﻿using System;
using System.Threading;
using System.Collections;

namespace MasterMind
{
    public class UserInt
    {
        // https://www.youtube.com/watch?v=sAn7baRbhx4
        private Logika logik;
        public UserInt(Logika logic)
        {
            logik = logic;
        }

        public int menu()
        {
            bool exit = true;
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Menu");
                Console.WriteLine("---------------------------");
                Console.WriteLine("1. Graj");
                Console.WriteLine("2. Ustawienia");
                Console.WriteLine("3. O tworcach");
                Console.WriteLine("4. Zakoncz");
                Console.WriteLine("---------------------------");
                string wybor = Console.ReadLine();
                Console.WriteLine(wybor);

                if (wybor is "1")
                {
                    graj();
                }
                else if (wybor is "2")
                {
                    this.ustawienia();
                }
                else if (wybor is "3")
                {
                    this.tworcy();
                }
                else if (wybor is "4")
                {
                    return 0;
                }
                else
                {
                    Console.WriteLine("Cos sie popsulo");
                    Console.Read();
                    Console.Clear();
                }

            }
            
        }
        //--------------------------------------------------------------------------------------
        public bool graj()
        {
            string helper;
            bool exit=false;
            int k=0;
            Console.Clear();

            for(int i=0; i<2;i++)
            {
            Console.WriteLine("Master Mind");
            Console.WriteLine("--------------------------------------");
                if (i == 0)
                {
                    Console.Write("Prosze podac Nazwe pierwszego gracza : ");
                    helper = Console.ReadLine();
                    // try catch dodac trzeba
                    logik.create_user(helper);
                    Console.Clear();
                }
                else
                {
                    Console.Write("Prosze podac Nazwe drugiego gracza : ");
                    helper = Console.ReadLine();
                    // try catch dodac trzeba
                    logik.create_user(helper);
                    Console.Clear();
                }
                
            }
            
            for (int j=0; j < logik.get_stages();j++)
            {
                this.tabela_win();
                this.stage0();
                this.stage1();
                if (logik.exit == true)
                {
                    logik.reset_all();
                    return true;
                }

                while (true)
                {
                    this.stage2();
                    if (logik.exit == true)
                    {
                        break;
                    }
                    Console.Clear();
                    if (logik.victory_by_guess() == true)
                    {
                        logik.get_g_2().add_pkt();
                        logik.revers_g_1_and_g_2();
                        break;
                    }
                    
                    this.stage3();
                    if (logik.exit == true)
                    {
                        break;
                    }

                    if (logik.victory_by_not_guess(k) == true)
                    {
                        logik.get_g_1().add_pkt();
                        logik.revers_g_1_and_g_2();
                        break;
                    }


                    k += 1;
                }
                if(logik.exit == true)
                {
                    logik.reset_all();
                    return true;
                }


                Console.Clear();
            }
            return false;

        }
        public void ustawienia()
        {
            int helper;
            
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Wybierz Liczbe tur: (Nie może to być liczba parzysta)");
                Console.WriteLine("Lub wyjdz wpisujac 0");
                try
                {
                    helper = Convert.ToInt32(Console.ReadLine());
                    if (helper == 0)
                    {
                        break;
                    }
                    else if (helper % 2 != 0)
                    {
                        logik.add_stages(Convert.ToInt32(helper));
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Nie wygłupiaj się i podaj poprawną liczbe(lub cyfre)");
                        Thread.Sleep(5000);
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Proprosze wpisac liczbe :C");
                    Thread.Sleep(5000);
                }
      
               
            }

        }
        public void tworcy()
        {
            Console.Clear();
            Console.WriteLine("Gra została stworzona na przedmiot Zastosowanie Programowanie obiektowego");
            Console.WriteLine("Stworzone przez:");
            Console.WriteLine("Błażeja");
            Console.WriteLine("Wojtka");
            Console.WriteLine("(Wiecej danych nie bedzie bo RODO :C)");
            Thread.Sleep(5000);

        }
        //--------------------------------------------------------------------------------------
        public void stage0()
        {
            Console.WriteLine("Teraz nastompi losowanie kolejnosci");
            Console.WriteLine("----------------------------------------------------------------");
            // Tu mozna wjebac animacje losowania 
            Thread.Sleep(2000);
            //Cos tu sie pierdoli 
            if(logik.losowanie() == 0)
            {
                Console.WriteLine("Ustalac kod bedzie {0}",logik.user1.see_name());
                logik.set_g_1(logik.user1);
            }
            else
            {
                Console.WriteLine("Ustalac kod bedzie {0}", logik.user2.see_name());
                logik.set_g_1(logik.user2);
            }
            Thread.Sleep(2000);

        }
        public void stage1()
        {
            Console.Clear();

            
            for(int i = 5;i>0;i--)
            {
                Console.WriteLine("Prosimy {0} o odwrocenie sie od ekranu", logik.get_g_2().see_name());
                Console.WriteLine("----------------------------------------------------------------");
                Console.WriteLine("-----------------------> {0} <-----------------------", i);
                Thread.Sleep(1000);
                Console.Clear();

            }
            logik.code.add(this.array_set(logik.get_g_1(), 0));
 
        }
        public void stage2()
        {
            Console.Clear();
            logik.mainlist.add(this.array_set(logik.get_g_2(), 1));

        }
        public bool stage3()
        {
            int[] ocena = new int[4];

            ocena[0] = 8;
            ocena[1] = 8;
            ocena[2] = 8;
            ocena[3] = 8;

            int[] help = (int[])logik.code.get_one(logik.code.lenght()- 1);
            int[] helpv2 = (int[])logik.mainlist.get_one(logik.mainlist.lenght() - 1);

            while (true)
            {
                Console.Clear();
                Console.WriteLine("----------------------------------------------------------------");
                Console.WriteLine("Tura gracza {0} ", logik.get_g_1().see_name());
                Console.WriteLine("----------------------------------------------------------------");
                Console.WriteLine("Zakodowany kod |{0}|{1}|{2}|{3}|", logik.main_colors.ret_by_string(help[0]), logik.main_colors.ret_by_string(help[1]), logik.main_colors.ret_by_string(help[2]), logik.main_colors.ret_by_string(help[3]));
                Console.WriteLine("----------------------------------------------------------------");
                Console.Write("Ostatni kod gracza |{0}|{1}|{2}|{3}|", logik.main_colors.ret_by_string(helpv2[0]), logik.main_colors.ret_by_string(helpv2[1]), logik.main_colors.ret_by_string(helpv2[2]), logik.main_colors.ret_by_string(helpv2[3]));
                Console.WriteLine("         |{0}|{1}|{2}|{3}|", logik.judge_colors.ret_by_string(ocena[0]), logik.judge_colors.ret_by_string(ocena[1]), logik.judge_colors.ret_by_string(ocena[2]), logik.judge_colors.ret_by_string(ocena[3]));
                Console.WriteLine("----------------------------------------------------------------");
                Console.WriteLine("1. {0}", logik.judge_colors.ret_by_string(ocena[0]));
                Console.WriteLine("2. {0}", logik.judge_colors.ret_by_string(ocena[1]));
                Console.WriteLine("3. {0}", logik.judge_colors.ret_by_string(ocena[2]));
                Console.WriteLine("4. {0}", logik.judge_colors.ret_by_string(ocena[3]));
                Console.WriteLine("help. pomoc");
                Console.WriteLine("save. Zapisz");
                Console.WriteLine("exit. Exit");

                string helper = Console.ReadLine();

                if (helper == "1")
                {
                    ocena[0] = this.menu_sprawdzenie();
                }
                else if (helper == "2")
                {
                    ocena[1] = this.menu_sprawdzenie();
                }
                else if (helper == "3")
                {
                    ocena[2] = this.menu_sprawdzenie();
                }
                else if (helper == "4")
                {
                    ocena[3] = this.menu_sprawdzenie();
                }
                else if (helper == "5")
                {
                    Console.Clear();
                    Console.WriteLine("pomoc");
                }
                else if (helper == "save")
                {
                    if (ocena[0] == 8)
                    {
                        ocena[0] = 0;
                    }
                    if (ocena[1] == 8)
                    {
                        ocena[1] = 0;
                    }
                    if (ocena[2] == 8)
                    {
                        ocena[2] = 0;
                    }
                    if (ocena[3] == 8)
                    {
                        ocena[3] = 0;
                    }

                    logik.listofanss.add(ocena);
                    return false;

                }
                else if(helper == "exit")
                {
                    return true;
                }
                else
                {
                    Console.WriteLine("tak nie mozna");
                }

            }

        }
        //Pomocnicze do stage--------------------------------------------------------------------
        public Array array_set(User loff, int h)
        {
            
            int heplful;
            int[] colorki = new int[4];

            colorki[0] = 0;
            colorki[1] = 0;
            colorki[2] = 0;
            colorki[3] = 0;

            while (true)
            {
                
                if(h == 1)
                {
                    this.see_game_table();
                }
                else
                {
                    Console.WriteLine("Stworz kod nie do zlamania");
                }
                Console.WriteLine("----------------------------------------------------------------");
                Console.WriteLine("Ustal kod {0}", loff.see_name());
                Console.WriteLine("----------------------------------------------------------------");
                Console.WriteLine("|{0}|{1}|{2}|{3}|", logik.main_colors.ret_by_string(colorki[0]), logik.main_colors.ret_by_string(colorki[1]), logik.main_colors.ret_by_string(colorki[2]), logik.main_colors.ret_by_string(colorki[3]));
                Console.WriteLine("----------------------------------------------------------------");

                Console.WriteLine("1. {0}", logik.main_colors.ret_by_string(colorki[0]));
                Console.WriteLine("2. {0}", logik.main_colors.ret_by_string(colorki[1]));
                Console.WriteLine("3. {0}", logik.main_colors.ret_by_string(colorki[2]));
                Console.WriteLine("4. {0}", logik.main_colors.ret_by_string(colorki[3]));
                Console.WriteLine("5. Pomoc");
                Console.WriteLine("save. Zapisz");
                Console.WriteLine("exit. wyjdz z gry");
                Console.WriteLine("----------------------------------------------------------------");
                string wybor = Console.ReadLine();


                if (wybor == "1")
                {
                    heplful = this.menu_color();

                    if (heplful != 0)
                    {
                        colorki[0] = heplful;
                    }
                    Console.Clear();
                }
                else if (wybor == "2")
                {
                    heplful = this.menu_color();

                    if (heplful != 0)
                    {
                        colorki[1] = heplful;
                        
                    }
                    Console.Clear();
                }
                else if (wybor == "3")
                {
                    heplful = this.menu_color();

                    if (heplful != 0)
                    {
                        colorki[2] = heplful;
                        
                    }
                    Console.Clear();
                }
                else if (wybor == "4")
                {
                    heplful = this.menu_color();

                    if (heplful != 0)
                    {
                        colorki[3] = heplful;
                        
                    }
                    Console.Clear();
                }
                else if (wybor == "5")
                {
                    Console.WriteLine("Tutaj bedzie pomoc w obsludze");
                }
                else if (wybor == "save")
                {
                    if ((colorki[0] != 0) && (colorki[1] != 0) && (colorki[2] != 0) && (colorki[3] != 0))
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Niestesty nie wszystkie kolorki zostaly wybrane");
                        Thread.Sleep(2000);
                        Console.Clear();
                    }

                }
                else if (wybor == "exit")
                {
                    logik.exit = true;
                    break;
                }
                else
                {
                    Console.Clear();
                }

            }

            Thread.Sleep(2000);
            return colorki;
        }
        public int menu_color()
        {
            string helper;

            while(true)
            {
                Console.Clear();
                Console.WriteLine("");
                Console.WriteLine("Kolorki do wyboru");
                Console.WriteLine("----------------------------------------------------------------");
                Console.WriteLine("1. Czerwony");
                Console.WriteLine("2. Niebieski");
                Console.WriteLine("3. Pomaranczowy");
                Console.WriteLine("4. Zielony");
                Console.WriteLine("5. Bialy");
                Console.WriteLine("6. Fioletowy");
                Console.WriteLine("7. Brazowy");
                Console.WriteLine("8. Zolty");
                helper = Console.ReadLine();

                if (helper == "1")
                {
                    return 1;
                }
                else if (helper == "2")
                {
                    return 2;
                }
                else if (helper == "3")
                {
                    return 3;
                }
                else if (helper == "4")
                {
                    return 4;
                }
                else if (helper == "5")
                {
                    return 5;
                }
                else if (helper == "6")
                {
                    return 6;
                }
                else if (helper == "7")
                {
                    return 7;
                }
                else if (helper == "8")
                {
                    return 8;
                }
                else if (helper == "Powrot")
                {
                    return 0;
                }
                else
                {
                    Console.WriteLine("Przepraszamy nie ma takiego koloru");
                    Thread.Sleep(2000);
                }
            }

            

        }
        public void see_game_table()
        {
            Array colorki;
            Array oceny;
           

            Console.WriteLine("----------------------------------------------------------------");
            Console.WriteLine("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
            Console.Clear();

            if (logik.mainlist.lenght() != 0)
            {
                
                for (int i = 0; i < logik.mainlist.lenght(); i++)
                {
                    colorki = logik.mainlist.get_one(i);
                    int color1 = (int)colorki.GetValue(0);
                    int color2 = (int)colorki.GetValue(1);
                    int color3 = (int)colorki.GetValue(2);
                    int color4 = (int)colorki.GetValue(3);

                    Console.Write("---------------------|{0}|{1}|{2}|{3}|---------------------", logik.main_colors.ret_by_string(color1), logik.main_colors.ret_by_string(color2), logik.main_colors.ret_by_string(color3), logik.main_colors.ret_by_string(color4));
                    try
                    {
                        oceny = logik.listofanss.get_one(i);
                        color1 = (int)oceny.GetValue(0);
                        color2 = (int)oceny.GetValue(1);
                        color3 = (int)oceny.GetValue(2);
                        color4 = (int)oceny.GetValue(3);
                        Console.WriteLine("-------------|{0}|{1}|{2}|{3}|---------------------", logik.main_colors.ret_by_string(color1), logik.main_colors.ret_by_string(color2), logik.main_colors.ret_by_string(color3), logik.main_colors.ret_by_string(color4));
                    }
                    catch (IndexOutOfRangeException)
                    {
                        Console.WriteLine("-------------|  |  |  |  |---------------------");
                    }

                    
                }
            }
            else
            {
                Console.WriteLine("---------------------| | | | |---------------------");
            }

            Console.WriteLine("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");

        }
        public void tabela_win()
        {

            //logik.user1.see_pkt() <= logik.user2.see_pkt())&& 
            Console.WriteLine("Tabela wynikow");
            Console.WriteLine("----------------------------------------------------------------");

            // To trzeba poprawic bo az zal dupe sciska jak sie na to patrzy
            if (logik.user1.see_pkt() < logik.user2.see_pkt())
            {
                Console.WriteLine("Nazwa gracza : {0} Liczba pkt {1} ", logik.user2.see_name(), logik.user2.see_pkt());
                Console.WriteLine("Nazwa gracza : {0} Liczba pkt {1} ", logik.user1.see_name(), logik.user1.see_pkt());

            }
            else
            {
                Console.WriteLine("Nazwa gracza : {0} Liczba pkt {1}", logik.user1.see_name(), logik.user1.see_pkt());
                Console.WriteLine("Nazwa gracza : {0} Liczba pkt {1}", logik.user2.see_name(), logik.user2.see_pkt());
            }
            Console.WriteLine("----------------------------------------------------------------");
            
            
        }
        public int menu_sprawdzenie()
        {
            string helper;

            while (true)
            {
                Console.Clear();
                Console.WriteLine("");
                Console.WriteLine("Kolorki do wyboru");
                Console.WriteLine("----------------------------------------------------------------");
                Console.WriteLine("1. Bialy");
                Console.WriteLine("2. Czerwony");
                Console.WriteLine("3. Pomoc");
                Console.WriteLine("exit. Wyjdz");
                
                helper = Console.ReadLine();

                if (helper == "1")
                {
                    return 1;
                }
                else if (helper == "2")
                {
                    return 2;
                }
                else if (helper == "3")
                {
                    Console.WriteLine("Pomoc tu bedzie");
                }
                else if (helper == "4")
                {
                    return 0;
                }
                else if (helper == "exit")
                {
                    return 0;
                }
                else
                {
                    Console.WriteLine("Przepraszamy nie ma takiego koloru");
                    Thread.Sleep(2000);
                }
            }



        }
        
        //--------------------------------------------------------------------------------------
        
        //--------------------------------------------------------------------------------------



    }
}

