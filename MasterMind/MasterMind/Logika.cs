﻿using System;
using System.Threading;

namespace MasterMind
{
    /// <summary>
    /// Main Mastermind class , in this class u can find objects of users, data containers which are important in program
    /// </summary>
    public class Logika
    {
        public bool exit = false;
        private int amount_of_stages = 3;

        private User g_1;
        private User g_2;

        public DictionaryClass main_colors;
        public DictionaryClass judge_colors;

        public BaseContainer code;
        public BaseContainer listofanss;
        public BaseContainer mainlist;

        public User user1;
        public User user2;

        public Logika()
        {
            this.main_colors = new DictionaryClass();
            this.judge_colors = new DictionaryClass();

            this.code = new BaseContainer(1);
            this.listofanss = new BaseContainer(10);
            this.mainlist = new BaseContainer(10);
            this.start_colors();
        }
        // amount methods ---------------------------------------------------------------------
        /// <summary>
        /// Sets amount of game stages
        /// </summary>
        /// <param name="num">must be int which is not even number</param>
        public void add_stages(int num)
        {
            if(num % 2 != 0)
            {
                this.amount_of_stages = num;
            }
            else
            {
                throw new Exception("Variable can not be even number");
            }
            
        }
        /// <summary>
        /// return amount of game stages
        /// </summary>
        /// <returns>int > 0</returns>
        public int get_stages()
        {
            return this.amount_of_stages;
        }
        /// <summary>
        /// sets amount of stages to 3
        /// </summary>
        public void reset_stages()
        {
            this.amount_of_stages = 3;
        }
        // victory condition ------------------------------------------------------------------
        /// <summary>
        /// check if code is the same as last array of mainlist ,if true code and mainlist is the same
        /// </summary>
        /// <returns>bool </returns>
        public bool victory_by_guess()
        {

            int[] helper = (int[])this.code.get_one(this.code.lenght() - 1);
            int[] helperv2 = (int[])this.mainlist.get_one(this.mainlist.lenght() - 1);


            if (helper[0] == helperv2[0]&& helper[1] == helperv2[1] && helper[2] == helperv2[2] && helper[3] == helperv2[3])
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        /// <summary>
        /// check amount of array in list if greater than 10 returns true
        /// </summary>
        /// <param name="n">Numbers of entered arrray to main list</param>
        /// <returns>bool</returns>
        public bool victory_by_not_guess(int n)
        {
            if(n == 10)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        // User classes -----------------------------------------------------------------------
        /// <summary>
        /// Create user1 if user1 exists create user2 if both exist rise error
        /// </summary>
        /// <param name="nazwa">name of user </param>
        public void create_user(string nazwa)// creates objects of User
        {
            if(user1 == null)
            {
                if(nazwa is string)
                {
                    this.user1 = new User(nazwa);
                }
                
            }
            else if(user2 == null)
            {
                if (nazwa is string)
                {
                    this.user2 = new User(nazwa);
                }
                
            }
            else
            {
                throw new Exception("Juz stworzona 2 obiekty tej klasy");
            }
        }
        // random generator  -----------------------------------------------------------------------------------
        /// <summary>
        /// random generator from 0 to 1
        /// </summary>
        /// <returns> int </returns>
        public int losowanie()// random generator from 0 to 1
        {
            Random rand = new Random();
            int wynik = rand.Next(0, 6);
            
            if(wynik % 2 == 0)
            {
                wynik = 0;
            }
            else
            {
                wynik = 1;
            }

            return wynik;
        }
        // methods for manageing g_1 and g_2 --------------------------------------------------------------------
        /// <summary>
        /// sets g_1 and g_2 , if user 1 is passed g_1 is user 1 and g_2 is user2
        /// </summary>
        /// <param name="g1"> user object </param>
        public void set_g_1(User g1)// give this method a object u want to be a g_1 and u have it bro
        {
            if (g1 == user1)
            {
                this.g_1 = this.user1;
                this.g_2 = this.user2;
            }
            else
            {
                this.g_1 = this.user2;
                this.g_2 = this.user1;
            }
        }
        /// <summary>
        /// returns object reference saved in g_1
        /// </summary>
        /// <returns>user object</returns>
        public User get_g_1() // gives u g_1
        {
            return this.g_1;
        }
        /// <summary>
        /// returns object reference saved in g_2
        /// </summary>
        /// <returns>user object</returns>
        public User get_g_2() // gives u g_2
        {
            return this.g_2;
        }
        /// <summary>
        /// g_1 is g_2 and g_2 is g_1 weird but it s usefull
        /// </summary>
        public void revers_g_1_and_g_2()// g_1 is g_2 and g_2 is g_1 weird but it s usefull
        {
            if(g_1 == this.user1)
            {
                g_1 = this.user2;
                g_2 = this.user1;
            }
            else
            {
                g_1 = this.user1;
                g_2 = this.user2;
            }
        }
        // private classes --------------------------------------------------------------------------------------
        /// <summary>
        /// docelowo tego nie bedzie ale potrzebujemy jakos dodac kolory
        /// </summary>
        private void start_colors()// docelowo tego nie bedzie ale potrzebujemy jakos dodac kolory
        {
            main_colors.add(0, "Nie podano");
            main_colors.add(1, "Czerwony    ");
            main_colors.add(2, "Niebieski   ");
            main_colors.add(3, "Pomaranczowy");
            main_colors.add(4, "Zielony     ");
            main_colors.add(5, "Bialy       ");
            main_colors.add(6, "Fioletowy   ");
            main_colors.add(7, "Brazowy     ");
            main_colors.add(8, "Zolty       ");

            judge_colors.add(0, "");
            judge_colors.add(1, "Bialy");
            judge_colors.add(2, "Czerwony");
            judge_colors.add(8, "Nie dodano");
        }

        public void reset_all()
        {
            reset_stages();
            g_1 = null;
            g_2 = null;
            user1 = null;
            user2 = null;

        }
    }

}

